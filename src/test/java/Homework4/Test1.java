package Homework4;

import com.codeborne.selenide.Configuration;
import lesson_4.steps.AuthorizationPageSteps;
import lesson_4.steps.CartPageFormSteps;
import lesson_4.steps.MainPageFormSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class Test1 {

    AuthorizationPageSteps authorizationPageSteps = new AuthorizationPageSteps();
    MainPageFormSteps mainPageFormSteps = new MainPageFormSteps();
    CartPageFormSteps cartPageFormSteps = new CartPageFormSteps();

    @BeforeClass
    public void setUp() {
        Configuration.timeout = 5000;
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://www.saucedemo.com/");
    }

    @Test
    public void test1() {
        //шаг 1 открываем сайт - ОР сайт открыт
        authorizationPageSteps.verifySite();
        //шаг 2 вводим логин - ОР логин введен
        authorizationPageSteps.writeLoginAndVerify("standard_user");
        //шаг 3 вводим пароль - ОР пароль введен
        authorizationPageSteps.writePasswordAndVerify("secret_sauce");
        //шаг 4 кликаем на кнопку логин - ОР открывается главная страница
        authorizationPageSteps.clickButtonAndVerifySite();

        //шаг 5 добавляем товар в корзину - ОР товар добавлен в корзину
        mainPageFormSteps.addToCart();
        //шаг 6 переходим в корзину - ОР открыта корзина
        mainPageFormSteps.openCart();
        // шаг 7 проверяем что товар находится в корзине - ОР товар находится в корзине
        cartPageFormSteps.VerCartList();
        //шаг 8 удаляем товар из корзины - ОР товар удален из корзины
        cartPageFormSteps.removeJacket();
        //Проверяем что корзина пустая - ОР корзина пустая
        cartPageFormSteps.checkCart();
    }
}
