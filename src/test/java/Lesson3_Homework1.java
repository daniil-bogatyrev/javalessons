import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import lesson_3_1.Rectangle;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;

public class Lesson3_Homework1 {
    @Test (groups = { "Rectangle" })
    public void getSquare(){
        Rectangle rec1 = new Rectangle(4, 7);
        Assert.assertEquals(rec1.getSquare(), 28);
    }
    @Test (groups = { "Rectangle" })
    public void getPerimeter(){
        Rectangle rec1 = new Rectangle(5, 8);
        Assert.assertEquals(rec1.getPerimeter(), 26);
    }
    @Test (groups = { "Rectangle" })
    public void getSquareSign(){
        Rectangle rec1 = new Rectangle(8, 8);
        Assert.assertTrue(rec1.getSquareSign());
    }
}
