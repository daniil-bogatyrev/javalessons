import lesson_3_2.Round;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Lesson3_Homework2 {
    boolean ratio;
    @Test
    public void getSquare(){
        Round round1 = new Round(5);
        Assert.assertEquals(round1.getSquare(), 78.53981633974483);
    }
    @Test
    public void getCircleLength(){
        Round round1 = new Round(2);
        Assert.assertEquals(round1.getCircleLength(), 6.283185307179586);
    }
    @Test
    public void ratio(){    //Тест проверяет больше ли площадь длины окружности
        Round round1 = new Round(2);
        ratio = round1.getSquare() > round1.getCircleLength();
        Assert.assertTrue(ratio);
    }
}
