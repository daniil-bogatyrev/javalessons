package Homework5;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import lesson_4.element.MainPageForm;
import lesson_4.steps.AuthorizationPageSteps;
import lesson_4.steps.CartPageFormSteps;
import lesson_4.steps.MainPageFormSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;

public class Homework5Test2 {
    AuthorizationPageSteps authorizationPageSteps = new AuthorizationPageSteps();
    MainPageFormSteps mainPageFormSteps = new MainPageFormSteps();
    CartPageFormSteps cartPageFormSteps = new CartPageFormSteps();

    @BeforeClass
    public void setUp() {
        Configuration.timeout = 5000;
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://www.saucedemo.com/");
    }

    @Test
    public void test1() {
        //шаг 1 открываем сайт - ОР сайт открыт
        authorizationPageSteps.verifySite();
        //шаг 2 вводим логин - ОР логин введен
        authorizationPageSteps.writeLoginAndVerify("standard_user");
        //шаг 3 вводим пароль - ОР пароль введен
        authorizationPageSteps.writePasswordAndVerify("secret_sauce");
        //шаг 4 кликаем на кнопку логин - ОР открывается главная страница
        authorizationPageSteps.clickButtonAndVerifySite();


        //шаг 5 добавляем несколько товаров в корзину через универсальный локатор
        mainPageFormSteps.addToCartButton("backpack").shouldBe(Condition.visible);
        mainPageFormSteps.addToCartButton("backpack").click();
        mainPageFormSteps.addToCartButton("fleece-jacket").shouldBe(Condition.visible);
        mainPageFormSteps.addToCartButton("fleece-jacket").click();
        mainPageFormSteps.addToCartButton("labs-bike").shouldBe(Condition.visible);
        mainPageFormSteps.addToCartButton("labs-bike").click();
        //шаг 6 проверяем что в корзине 3 элемента
        MainPageForm.cartIndicator.shouldBe(text("3"));
        //шаг 7 переходим в корзину
        mainPageFormSteps.openCart();
        // шаг 8 проверяем что товары находятся в корзине
        cartPageFormSteps.checkName("Backpack").shouldBe(visible);
        cartPageFormSteps.checkName("Fleece Jacket").shouldBe(visible);
        cartPageFormSteps.checkName("Bike Light").shouldBe(visible);
        //шаг 9 удаляем товар из корзины - ОР товар удален из корзины
        cartPageFormSteps.removeFromCart("backpack").click();
        cartPageFormSteps.removeFromCart("fleece-jacket").click();
        cartPageFormSteps.removeFromCart("labs-bike").click();
        //шаг 10 Проверяем что корзина пустая - ОР корзина пустая
        cartPageFormSteps.checkCart();
    }
}