package Homework5;

import lesson_3_1.Rectangle;
import lesson_3_2.Round;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Homework5Test1 {

    @Test
    public void getRoundCoordinates() {
        Round round = new Round(4);
        round.moveLeft(10);
        round.moveRight(11);
        round.moveDown(5);
        round.moveUp(8);
        round.getСoordinates();
        Assert.assertEquals(round.x, 1);
        Assert.assertEquals(round.y, 3);
    }
    @Test
    public void getRectangleCoordinates(){
        Rectangle rectangle = new Rectangle(4,5);
        rectangle.moveLeft(5);
        rectangle.moveDown(3);
        rectangle.moveRight(3);
        rectangle.moveUp(12);
        rectangle.getСoordinates();
        Assert.assertEquals(rectangle.x, -2);
        Assert.assertEquals(rectangle.y, 9);

    }
}




