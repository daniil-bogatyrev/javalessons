package lesson_2;

import java.util.LinkedList;

public class Homework6 {
    public static void main(String[] args) {
        String str1 = new String("Привет! ");
        String str2 = new String("Это сообщение ");
        String str3 = new String("выводится с помощью ");
        String str4 = new String("LinkedList!");
        LinkedList<String> list = new LinkedList<String>();
        list.add(str1);
        list.add(str2);
        list.add(str3);
        list.add(str4);
        int size = list.size();
        for (int i = 0; i < size; i++){
            System.out.print(list.get(i));
        }

    }

}
