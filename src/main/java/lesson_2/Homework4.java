package lesson_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Homework4 {
    public static void main(String[] args) {
        List<Integer> posled = new ArrayList<Integer>();
        int j = 1;
        for (int i = 1; i <= 20; i++) {
            j*=2;
            posled.add(j);
        }
        System.out.println("Массив содержит 20 элементов.");
        Scanner in = new Scanner(System.in);;
        System.out.print("Введите номер элемента: ");
        int num = in.nextInt();
        System.out.println("Значение элемента массива: " + posled.get(num));
    }
}
