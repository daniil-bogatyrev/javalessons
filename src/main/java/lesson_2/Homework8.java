package lesson_2;

public class Homework8 {
    public static void main(String[] args) {
        int array[] = new int[10];
        for (int i = 0; i < 10; i++){
            array[i] = i;
        }
        for (int j = 0; j < array.length; j++){
            System.out.println(array[j]);
        }

        System.out.println();


        int n = array.length;
        int temp;
        for (int i = 0; i < n / 2; i++){
            temp = array[n-i-1];
            array[n-i-1] = array[i];
            array[i] = temp;
        }

        for (int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }
     /*
        0 [9, 1, 2, 3, 4, 5, 6, 7, 8, 0]
        1 [9, 8, 2, 3, 4, 5, 6, 7, 1, 0]
        2 [9, 8, 7, 3, 4, 5, 6, 2, 1, 0]
    */
    }
}
