package lesson_2;

import java.util.*;

public class Homework5 {
    public static void main(String[] args) {
        Map<Integer, Integer> map = new HashMap();
        System.out.println("Программа позволяет заполнять коллекцию Map, если вы законили заполнять коллекцию, введите '-999'");
        int key = 0;
        while (key != -999) {
            Scanner in = new Scanner(System.in);
            System.out.print("Введите ключ элемента: ");
            key = in.nextInt();
            if (key == -999){
                break;
            }
            Scanner in2 = new Scanner(System.in);
            System.out.print("Введите значение элемента: ");
            int val = in.nextInt();
            map.put(key, val);
            Set<Integer> keys = map.keySet();
            Iterator<Integer> iterator = keys.iterator();
            System.out.println("Элементы списка: ");
            while (iterator.hasNext()) {
                System.out.println(map.get(iterator.next()));
            }
            System.out.println("Размер массива: " + map.size() + " элемент(ов)");
            System.out.println();

        }
    }
}
