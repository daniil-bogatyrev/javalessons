package lesson_2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class homework3 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();  //создал массив
        list.add(12);      //заполняю
        list.add(13);
        list.add(2);
        list.add(2412);
        list.add(2465);
        list.add(9);
        list.add(34);
        list.add(345);
        for (int i = 0; i < 8; i++){
            System.out.println(list.get(i));
        }
        System.out.println();
        list.set(0, 66);    //Заменяю первый элемент массива
        list.remove(7); //Удаляю последний элемент массива
        System.out.println(list.size()); //Вывожу размер массива
        System.out.println();
        list.remove(6);
        list.remove(5);
        for (int i = 0; i < 5; i++){
            System.out.println(list.get(i));
        }


    }
}
