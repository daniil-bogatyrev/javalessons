package lesson_3_2;

import lesson_5.Figure;

public class Round extends Figure {
    int radius;
    double square;
    double length;

    public Round(int radius){
        this.radius = radius;
    }
    public double getSquare(){
        square = Math.PI * radius * radius;
        System.out.println("Square is: " + square);
        return square;
    }
    public double getCircleLength(){
        length = Math.PI * radius;
        System.out.println("Circle length is: " + length);
        return length;
    }

    @Override
    public void getСoordinates() {
        System.out.println("Координаты окружности " + "(" + x + "," + y + ")");
    }

    @Override
    public void area() {}

    @Override
    public void length() {}
}
