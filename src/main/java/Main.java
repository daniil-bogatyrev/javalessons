import lesson_5.MovableObjects;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //определяю объекты
        MovableObjects cat = new MovableObjects();
        MovableObjects round = new MovableObjects();
        MovableObjects wolf = new MovableObjects();
        MovableObjects hexagon = new MovableObjects();
        MovableObjects oval = new MovableObjects();
        //создаю и заполняю коллекцию объектами
        List<MovableObjects> list1 = new ArrayList<MovableObjects>();
        list1.add(cat);
        list1.add(round);
        list1.add(wolf);
        list1.add(hexagon);
        list1.add(oval);
        //перемещаю объекты
        for (int i = 0; i < list1.size(); i++){
            list1.get(i).moveRight(10);
        }
        //получаю координаты
        cat.getСoordinates();
        round.getСoordinates();
        wolf.getСoordinates();
        hexagon.getСoordinates();
        oval.getСoordinates();






    }
}
