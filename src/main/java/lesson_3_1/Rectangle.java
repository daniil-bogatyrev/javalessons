package lesson_3_1;

import lesson_5.Figure;

public class Rectangle extends Figure {
    int width;
    int height;
    int square;
    int perimeter;
    boolean squareSign;
    public Rectangle(int width, int height){
        this.width = width;
        this.height = height;
    }
    public int getSquare(){
        square = width * height;
        System.out.println("Square is: " + square);
        return square;
    }
    public int getPerimeter(){
        perimeter = 2*(width + height);
        System.out.println("Perimeter is: " + perimeter);
        return perimeter;
    }
    public boolean getSquareSign(){
        if (width == height) {
            System.out.println("Figure is square");
            return squareSign = true;
        } else {
            System.out.println("Figure is not square");
            return squareSign = false;
        }
    }
    @Override
    public void getСoordinates() {
        System.out.println("Координаты прямоугольника " + "(" + x + "," + y + ")");
    }

    @Override
    public void area() {
        System.out.println("Area");
    }

    @Override
    public void length() {
        System.out.println("Lenght");
    }
}
