package lesson_4.steps;

import com.codeborne.selenide.SelenideElement;
import lesson_4.element.MainPageForm;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.webdriver;
import static com.codeborne.selenide.WebDriverConditions.url;

public class MainPageFormSteps {
    //Шаг 1. Добавляем fleeceJacket в корзину ОР Товар добавлен, индикатор товаров в корзине = 1
    public void addToCart(){
        MainPageForm.fleeceJacket.shouldBe(visible);
        MainPageForm.fleeceJacket.click();
        MainPageForm.remFleeceJacket.shouldBe(visible);
        MainPageForm.cartIndicator.shouldBe(text("1"));
    }

    //Шаг 2. Открываем корзину
    public void openCart(){
        MainPageForm.cart.shouldBe(visible);
        MainPageForm.cart.click();
        webdriver().shouldHave(url("https://www.saucedemo.com/cart.html"));
    }
    //Шаг 3. Добавляем товары в корзину  через универсальный локатор
    public SelenideElement addToCartButton (String name){
        return $x("//button[contains(@id, '" + name + "')]");
    }




}
