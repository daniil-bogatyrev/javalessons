package lesson_4.steps;

import com.codeborne.selenide.Condition;
import lesson_4.element.AuthorizationPageForm;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.webdriver;
import static com.codeborne.selenide.WebDriverConditions.url;


public class AuthorizationPageSteps {

    AuthorizationPageForm authorizationPageForm = new AuthorizationPageForm();

    //шаг 1 открываем сайт - ОР сайт открыт
    public void verifySite() {
        authorizationPageForm.logotype.shouldBe(Condition.visible);
        webdriver().shouldHave(url("https://www.saucedemo.com/"));
    }

    //шаг 2 вводим логин - ОР логин введен
    public void writeLoginAndVerify(String login) {
        authorizationPageForm.login.shouldBe(visible);
        authorizationPageForm.login.setValue(login);
        authorizationPageForm.login.shouldHave(value(login));
    }

    //шаг 3 вводим пароль - ОР пароль введен
    public void writePasswordAndVerify(String password) {
        authorizationPageForm.password.shouldBe(visible);
        authorizationPageForm.password.setValue(password);
        authorizationPageForm.password.shouldHave(value(password));
    }

    //шаг 4 кликаем на кнопку логин - ОР открывается главная страница
    public void clickButtonAndVerifySite() {
        authorizationPageForm.buttonLogin.shouldBe(visible).click();
        authorizationPageForm.header.shouldBe(appear);
        authorizationPageForm.header.shouldHave(text("Products"));
        webdriver().shouldHave(url("https://www.saucedemo.com/inventory.html"));
    }
}
