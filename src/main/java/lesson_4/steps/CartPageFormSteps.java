package lesson_4.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lesson_4.element.CartPageForm;
import lesson_4.element.MainPageForm;

import static com.codeborne.selenide.Selenide.$x;

public class CartPageFormSteps {
    //Проверка что в корзину добавился именно Fleece Jacket
    public void VerCartList(){
        CartPageForm.fJacketButton.shouldBe(Condition.text("Sauce Labs Fleece Jacket"));
    }
    public void VerCartList(String text){
        CartPageForm.fJacketButton.shouldBe(Condition.text("'" + text + "'"));
    }
    //Удаляем товар из корзины
    public void removeJacket(){
        CartPageForm.removeFJacketButton.shouldBe(Condition.visible);
        CartPageForm.removeFJacketButton.click();
    }
    //Проверяем что корзина пустая
    public void checkCart(){
        CartPageForm.fJacketButton.shouldNotBe(Condition.exist);
        MainPageForm.cartIndicator.shouldNotBe(Condition.exist);
    }
    //Проверяем через универсальный локатор поля с названиями товаров в корзине
    public SelenideElement checkName (String name){
        return $x("//div[contains(text(), '" + name + "')]");
    }
    //Кнопка удаления товара по через универсальный локатор
    public SelenideElement removeFromCart (String name){
        return $x("//button[contains(@id, '" + name + "')]");
    }
}
