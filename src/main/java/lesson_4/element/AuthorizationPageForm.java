package lesson_4.element;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class AuthorizationPageForm {
    //логотип
    public SelenideElement logotype = $("[class=login_logo]");

    //логин
    public SelenideElement login = $("#user-name");

    //пароль
    public SelenideElement password = $x("//*[contains(@class,'input_error') and @id='password']");

    //кнопка
    public SelenideElement buttonLogin = $("input.submit-button.btn_action");

    //заголовок
    public SelenideElement header = $("span.title");
}
