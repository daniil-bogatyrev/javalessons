package lesson_4.element;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class MainPageForm {
    //Sauce Labs Fleece Jacket
    public static SelenideElement fleeceJacket = $("[name=add-to-cart-sauce-labs-fleece-jacket]");
    //Кнопка удаления из корзины
    public static SelenideElement remFleeceJacket = $("[name=remove-sauce-labs-fleece-jacket]");
    //Индкатор товаров в корзине
    public static SelenideElement cartIndicator = $("[class=shopping_cart_badge]");
    //Кнопка перехода в корзину
    public static SelenideElement cart =  $("[class=shopping_cart_link]");

}
