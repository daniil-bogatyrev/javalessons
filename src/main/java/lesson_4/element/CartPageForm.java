package lesson_4.element;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CartPageForm {
    //кнопка с названием товара в корзине
    public static SelenideElement fJacketButton = $x("//div[contains(text(), 'Fleece Jacket')]");
    //кнопка удаления товара из корзины
    public static SelenideElement removeFJacketButton = $("[name = remove-sauce-labs-fleece-jacket]");
}
