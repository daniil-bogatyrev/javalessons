package lesson_1;

import java.util.Scanner;

public class Homework5 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);;
        System.out.print("Введите сторону квадрата: ");
        int Side = in.nextInt();
        int Perimeter = 4 * Side;
        int Square = Side * Side;
        System.out.println("Периметр квадрата = " + Perimeter);
        System.out.println("Площадь квадрата = " + Square);
    }
}
