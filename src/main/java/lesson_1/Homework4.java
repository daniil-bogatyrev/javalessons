package lesson_1;

public class Homework4 {
    public static void main (String[] args){
        int[] array = new int [10];  //int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        for (int j = 0; j < 10; j++) { // Заполняю массив циклом
            array[j] = j + 1;
        }

        for (int i = 9; i >= 0; i--) { // Циклом вывожу его в обратном порядке
            System.out.println(array[i]);
        }

    }
}
