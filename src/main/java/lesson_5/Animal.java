package lesson_5;

public class Animal implements Movable{
    public String name;
    int x;
    int y;

    public String setName(String name){
        this.name = name;
        return name;
    }

    @Override
    public int moveLeft(int x) {
        this.x = this.x - x;
        return this.x;
    }

    @Override
    public int moveRight(int x) {
        this.x = this.x + x;
        return this.x;
    }

    @Override
    public int moveDown(int x) {
        this.y = this.y - y;
        return this.y;
    }

    @Override
    public int moveUp(int x) {
        this.y = this.y + y;
        return this.y;
    }
}
