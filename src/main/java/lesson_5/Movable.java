package lesson_5;

public interface Movable {
    int moveLeft(int x);
    int moveRight(int x);
    int moveDown(int x);
    int moveUp(int x);

}
