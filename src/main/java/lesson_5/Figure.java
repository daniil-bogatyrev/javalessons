package lesson_5;

public abstract class Figure implements Movable{

    public int x = 0;
    public int y = 0;

    public void setColor (String color){
        System.out.println("Figure color is: " + color);
    }

    public void getСoordinates(){
        System.out.println("Координата х = " + x);
        System.out.println("Координата y = " + y);
    }
    @Override
    public int moveLeft(int x){
        this.x = this.x - x;
        return this.x;
    }
    @Override
    public int moveRight(int x){
        this.x = this.x + x;
        return this.x;
    }
    @Override
    public int moveDown(int y){
        this.y = this.y - y;
        return this.y;
}
    @Override
    public int moveUp(int y){
        this.y = this.y + y;
        return this.y;
    }

    public abstract void area();
    public abstract void length();

}
