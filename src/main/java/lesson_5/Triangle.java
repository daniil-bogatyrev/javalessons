package lesson_5;

public class Triangle extends Figure {
    int firstSide;
    int secondSide;
    int thirdSide;
    public Triangle(int firstSide, int secondSide, int thirdSide){
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;
    }

    @Override
    public void area() {
        double a = (firstSide * secondSide)/2;
        System.out.println("Area is: " + a);
    }

    @Override
    public void length() {
        int a = firstSide + secondSide + thirdSide;
        System.out.println("Length is: " + a);
    }
}
